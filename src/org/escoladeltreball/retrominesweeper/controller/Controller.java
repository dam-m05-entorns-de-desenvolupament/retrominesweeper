/*
 * Copyright 2013 Mònica Ramírez Arceda <mramirez@escoladeltreball.org>
 *
 * This file is part of retrominesweeper.
 *
 * retrominesweeper is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * retrominesweeper is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with retrominesweeper.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.escoladeltreball.retrominesweeper.controller;

import org.escoladeltreball.retrominesweeper.model.Game;
import org.escoladeltreball.retrominesweeper.view.MinesweeperView;

/**
 * @version Jan 13, 2013
 * @author Mònica Ramírez Arceda
 */
public class Controller {

    private MinesweeperView minesweeperView;

    public Controller(MinesweeperView minesweeperView) {
	this.minesweeperView = minesweeperView;
    }

    public void play() {
	Game game = new Game();
	boolean boom = false;
	boolean win = false;
	while (!boom && !win) {
	    this.minesweeperView.showPlayerBoard(game.getPlayerBoard());
	    String[] option = this.minesweeperView.askOption();
	    boom = game.processOption(option);
	    win = game.hasWon();
	}
	if (win)
	    System.out.println("Molt bé!!! Has guanyat!!!");
	else
	    System.out.println("BOOOM!!!");
	game.putBombsInPlayerBoard();
	this.minesweeperView.showPlayerBoardWithBombs(game.getPlayerBoard());
    }

}
