/*
 * Copyright 2013 Mònica Ramírez Arceda <mramirez@escoladeltreball.org>
 *
 * This file is part of retrominesweeper.
 *
 * retrominesweeper is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * retrominesweeper is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with retrominesweeper.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.escoladeltreball.retrominesweeper.view;

import java.util.Scanner;

import org.escoladeltreball.retrominesweeper.controller.Controller;


/**
 * @version Jan 12, 2013
 * @author Mònica Ramírez Arceda
 */
public class MinesweeperView {

    /** The controller */
    private Controller c;
    /** The scanner to collect gamer input */
    private Scanner sc;

    public MinesweeperView() {
	sc = new Scanner(System.in);
	this.c = new Controller(this);
	c.play();
	sc.close();
    }

    public void showPlayerBoard(char[][] playerBoard) {
	System.out.print("    ");
	for (int i = 0; i < playerBoard[0].length; i++) {
	    System.out.print(i+" ");
	}
	System.out.println();
	System.out.println();
	for (int i = 0; i < playerBoard.length; i++) {
	    System.out.print(i+"   ");
	    for (int j = 0; j < playerBoard[0].length; j++) {
		if (playerBoard[i][j] == '*')
		    System.out.print("# ");
		else
		    System.out.print(playerBoard[i][j]+" ");
	    }
	    System.out.println();
	}
    }

    public void showPlayerBoardWithBombs(char[][] playerBoard) {
	System.out.print("    ");
	for (int i = 0; i < playerBoard.length; i++) {
	    System.out.print(i + " ");
	}
	System.out.println();
	System.out.println();
	for (int i = 0; i < playerBoard.length; i++) {
	    System.out.print(i + "   ");
	    for (int j = 0; j < playerBoard[0].length; j++) {
		System.out.print(playerBoard[i][j] + " ");
	    }
	    System.out.println();
	}
    }

    public String[] askOption() {
	System.out.print("Acció (b-fila-columna o d-fila-columna) : ");
	String optionS = sc.next();
	String[] optionA = optionS.split("-");
	return optionA;
    }

    public static void main(String[] args) {
	new MinesweeperView();
    }

}
