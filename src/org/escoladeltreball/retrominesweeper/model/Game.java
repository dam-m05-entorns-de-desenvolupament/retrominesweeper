/*
 * Copyright 2013 Mònica Ramírez Arceda <mramirez@escoladeltreball.org>
 *
 * This file is part of retrominesweeper.
 *
 * retrominesweeper is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * retrominesweeper is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with retrominesweeper.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.escoladeltreball.retrominesweeper.model;

import java.util.Random;

/**
 * @version Jan 12, 2013
 * @author Mònica Ramírez Arceda
 */
public class Game {

    private char[][] playerBoard;
    private boolean[][] mineBoard;
    private final static int ROWS = 5;
    private final static int COLS = 8;
    private final static int MINES = 10;

    public Game() {
	this.playerBoard = new char[ROWS][COLS];
	this.mineBoard = new boolean[ROWS][COLS];
	for (int i = 0; i < playerBoard.length; i++) {
	    for (int j = 0; j < playerBoard[0].length; j++) {
		this.playerBoard[i][j] = '#';
		this.mineBoard[i][j] = false;
	    }
	}

	for (int i = 0; i < MINES; i++) {
	    Random r = new Random();
	    int mx = r.nextInt(ROWS);
	    int my = r.nextInt(COLS);
	    while (this.playerBoard[mx][my] == '*') {
		mx = r.nextInt(ROWS);
		my = r.nextInt(COLS);
	    }
	    this.playerBoard[mx][my] = '*';
	    this.mineBoard[mx][my] = true;
	}
    }

    /**
     * Gets the mineBoard.
     * 
     * @return the mineBoard
     */
    public char[][] getPlayerBoard() {
	return playerBoard;
    }

    public boolean processOption(String[] option) {
	boolean boom = false;
	char whatToDo = option[0].trim().charAt(0);
	int row = Integer.parseInt(option[1]);
	int col = Integer.parseInt(option[2]);
	if (whatToDo == 'b') {
	    if (this.playerBoard[row][col] == 'b') {
		this.playerBoard[row][col] = '#';
	    } else if (this.playerBoard[row][col] == '#'
		    || this.playerBoard[row][col] == '*') {
		this.playerBoard[row][col] = 'b';
	    }
	} else {
	    if (this.mineBoard[row][col]) {
		boom = true;
	    } else {
		this.noBombs(row, col);
	    }
	}
	return boom;
    }

    private void noBombs(int row, int col) {
	int bombs = this.calculateHowManyBombsNextTo(row, col);
	if (bombs == 0 && this.playerBoard[row][col] != '·'
		&& this.playerBoard[row][col] != 'b') {
	    this.playerBoard[row][col] = '·';
	    if (col < COLS - 1)
		this.noBombs(row, col + 1);
	    if (col > 0)
		this.noBombs(row, col - 1);
	    if (row < ROWS - 1)
		this.noBombs(row + 1, col);
	    if (row > 0)
		this.noBombs(row - 1, col);
	    if (row > 0 && col > 0)
		this.noBombs(row - 1, col - 1);
	    if (col < COLS - 1 && row > 0)
		this.noBombs(row - 1, col + 1);
	    if (row < ROWS - 1 && col > 0)
		this.noBombs(row + 1, col - 1);
	    if (row < ROWS - 1 && col < COLS - 1)
		this.noBombs(row + 1, col + 1);
	} else if (bombs != 0 && this.playerBoard[row][col] != 'b') {
	    this.playerBoard[row][col] = String.valueOf(bombs).charAt(0);
	}

    }

    private int calculateHowManyBombsNextTo(int row, int col) {
	int bombs = 0;
	if (col < COLS - 1 && this.mineBoard[row][col + 1])
	    bombs++;
	if (col > 0 && this.mineBoard[row][col - 1])
	    bombs++;
	if (row < ROWS - 1 && this.mineBoard[row + 1][col])
	    bombs++;
	if (row > 0 && this.mineBoard[row - 1][col])
	    bombs++;
	if (row > 0 && col > 0 && this.mineBoard[row - 1][col - 1])
	    bombs++;
	if (col < COLS - 1 && row > 0 && this.mineBoard[row - 1][col + 1])
	    bombs++;
	if (row < ROWS - 1 && col > 0 && this.mineBoard[row + 1][col - 1])
	    bombs++;
	if (row < ROWS - 1 && col < COLS - 1
		&& this.mineBoard[row + 1][col + 1])
	    bombs++;
	return bombs;

    }

    public boolean hasWon() {
	int good = 0;
	for (int i = 0; i < playerBoard.length; i++) {
	    for (int j = 0; j < playerBoard[0].length; j++) {
		if (playerBoard[i][j] == 'b' && mineBoard[i][j])
		    good++;
	    }
	}
	return good == MINES;
    }

    public char[][] putBombsInPlayerBoard() {
	for (int i = 0; i < playerBoard.length; i++) {
	    for (int j = 0; j < playerBoard[0].length; j++) {
		if (mineBoard[i][j])
		    playerBoard[i][j] = '*';

	    }
	}
	return null;
    }

}
